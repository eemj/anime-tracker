package main

import (
	"log"
	"os"

	"gitlab.com/eemj/anime-tracker/backend/server"
)

type Release struct {
	Name    string `json:"name"`
	Episode string `json:"episode"`
	Watched bool   `json:"watched"`
}

func main() {
	if port := os.Getenv("PORT"); port == "" {
		err := os.Setenv("PORT", "8999")

		if err != nil {
			log.Fatal(err)
		}
	}

	conn := server.DBConnection{
		Host:     os.Getenv("DATABASE_HOST"),
		Port:     os.Getenv("DATABASE_PORT"),
		DBName:   os.Getenv("DATABASE_NAME"),
		User:     os.Getenv("DATABASE_USER"),
		Password: os.Getenv("DATABASE_PASS"),
	}

	srv, err := server.NewServer(conn)

	if err != nil {
		log.Fatal(err)
	}

	defer srv.DB.Close()

	log.Printf("Listening on :%s\n", os.Getenv("PORT"))

	err = srv.Run()

	if err != nil {
		log.Fatal(err)
	}
}
