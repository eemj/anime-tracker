package test

import (
	"net/url"
	"strconv"
)

type TestScenario struct {
	expectedStatusCode int
	requestBody interface{}
	responseBody string
}

type AnimeCreation struct {
	Name string `json:"name,omitempty"`
	Link string `json:"link,omitempty"`
	Image string `json:"image,omitempty"`
	Status string `json:"status,omitempty"`
}

func (a AnimeCreation) IntoForm() string {
	form := url.Values{}
	if len(a.Name) > 0 {
		form.Set("name", a.Name)
	}
	if len(a.Link) > 0 {
		form.Set("link", a.Link)
	}
	if len(a.Image) > 0 {
		form.Set("image", a.Image)
	}
	if len(a.Status) > 0 {
		form.Set("status", a.Status)
	}
	return form.Encode()
}

type ReleaseCreation struct {
	Episode string `json:"episode,omitempty"`
	DateReleased int64 `json:"date_released,omitempty"`
}

func (r ReleaseCreation) IntoForm() string {
	form := url.Values{}
	if len(r.Episode) > 0 {
		form.Set("episode", r.Episode)
	}
	if r.DateReleased > 0 {
		form.Set("date_released", strconv.FormatInt(r.DateReleased, 10))
	}
	return form.Encode()
}

type AnimeUpdate struct {
	Status string `json:"status,omitempty"`
}

func (a AnimeUpdate) IntoForm() string {
	form := url.Values{}
	if len(a.Status) > 0 {
		form.Set("status", a.Status)
	}
	return form.Encode()
}

type ReleaseUpdate struct {
	Watched bool `json:"watched"`
}

func (r ReleaseUpdate) IntoForm() string {
	form := url.Values{}
	form.Set("watched", strconv.FormatBool(r.Watched))
	return form.Encode()
}
