package test

import (
	"log"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/eemj/anime-tracker/backend/routes"
)

var s *TestServer

func TestCreateAnime(t *testing.T) {
	defer s.CleanDatabase()

	scenarios := []TestScenario{
		TestScenario{
			expectedStatusCode: http.StatusCreated,
			requestBody: AnimeCreation{
				Name:   "One Piece",
				Link:   "one-piece",
				Status: "Finished",
				Image:  "http://something.com/one-piece.jpg",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusCreated,
			requestBody: AnimeCreation{
				Name:   "Naruto",
				Link:   "naruto",
				Status: "Finished",
				Image:  "http://something.com/naruto.jpg",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusConflict,
			requestBody: AnimeCreation{
				Name:   "One Piece",
				Link:   "one-piece",
				Status: "Finished",
				Image:  "http://something.com/one-piece.jpg",
			},
			responseBody: routes.AnimeExists,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name:  "One Punch Man",
				Image: "http://something.com/one-punch-man.jpg",
			},
			responseBody: routes.MissingLink,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Link:  "one-punch-man",
				Image: "http://something.com/one-punch-man.jpg",
			},
			responseBody: routes.MissingName,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name: "One Punch Man",
				Link: "one-punch-man",
			},
			responseBody: routes.MissingImage,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name:  "One Punch Man",
				Link:  "one-punch-man",
				Image: "http://something.com/one-punch-man",
			},
			responseBody: routes.ImageIsNotValidURI,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name:  "One Punch Man",
				Link:  "one-punch-man",
				Image: "http://something.com/one-punch-man.mp4",
			},
			responseBody: routes.ImageIsNotValidURI,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name:  "One Punch Man",
				Link:  "one-punch-man",
				Image: "http://something.com/one-punch-man.webm",
			},
			responseBody: routes.ImageIsNotValidURI,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeCreation{
				Name:  "One Punch Man",
				Link:  "one-punch-man",
				Image: "http://something.com/one-punch-man.webp",
			},
			responseBody: routes.ImageIsNotValidURI,
		},
	}

	RunScenarios(scenarios, t)
}

func TestCreateRelease(t *testing.T) {
	defer s.CleanDatabase()

	now := time.Now().Unix()

	scenarios := []TestScenario{
		TestScenario{
			expectedStatusCode: http.StatusCreated,
			requestBody: ReleaseCreation{
				DateReleased: now,
				Episode:      "1",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusCreated,
			requestBody: ReleaseCreation{
				DateReleased: now,
				Episode:      "2",
			},
		},
		// in the run scenarios we're currently assigning releases according to the remainder of the index divided by the animeIDS
		TestScenario{
			expectedStatusCode: http.StatusConflict,
			requestBody: ReleaseCreation{
				DateReleased: now,
				Episode:      "1",
			},
			responseBody: routes.ReleaseExists,
		},
		TestScenario{
			expectedStatusCode: http.StatusCreated,
			requestBody: ReleaseCreation{
				DateReleased: now,
				Episode:      "3",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: ReleaseCreation{
				DateReleased: now,
			},
			responseBody: routes.MissingEpisode,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: ReleaseCreation{
				Episode: "1",
			},
			responseBody: routes.MissingDateReleased,
		},
	}

	RunScenarios(scenarios, t)
}

func TestDeleteAnime(t *testing.T) {
	defer s.CleanDatabase()

	scenarios := []TestScenario{
		{
			expectedStatusCode: http.StatusAccepted,
			responseBody: "OK",
		},
		{
			expectedStatusCode: http.StatusAccepted,
			responseBody: "OK",
		},
		{
			expectedStatusCode: http.StatusNotFound,
			responseBody: routes.AnimeDoesNotExist,
		},
	}

	RunScenarios(scenarios, t)
}

func TestDeleteRelease(t *testing.T) {
	defer s.CleanDatabase()

	scenarios := []TestScenario{
		{
			expectedStatusCode: http.StatusAccepted,
			responseBody: "OK",
		},
		{
			expectedStatusCode: http.StatusAccepted,
			responseBody: "OK",
		},
		{
			expectedStatusCode: http.StatusNotFound,
			responseBody: routes.AnimeDoesNotExist,
		},
		{
			expectedStatusCode: http.StatusNotFound,
			responseBody: routes.ReleaseDoesNotExist,
		},
	}

	RunScenarios(scenarios, t)
}

func TestUpdateAnime(t *testing.T) {
	defer s.CleanDatabase()

	scenarios := []TestScenario{
		TestScenario{
			expectedStatusCode: http.StatusAccepted,
			requestBody: AnimeUpdate{
				Status: "finished",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusAccepted,
			requestBody: AnimeUpdate{
				Status: "ongoing",
			},
		},
		TestScenario{
			expectedStatusCode: http.StatusConflict,
			requestBody: AnimeUpdate{
				Status: "finished",
			},
			responseBody: routes.StatusDidNotChange,
		},
		TestScenario{
			expectedStatusCode: http.StatusBadRequest,
			requestBody: AnimeUpdate{},
			responseBody: routes.MissingStatus,
		},
	}

	RunScenarios(scenarios, t)
}

func TestUpdateRelease(t *testing.T) {
	defer s.CleanDatabase()

	scenarios := []TestScenario{
		{
			expectedStatusCode: http.StatusAccepted,
			requestBody: ReleaseUpdate{
				Watched: true,
			},
		},
		{
			expectedStatusCode: http.StatusAccepted,
			requestBody: ReleaseUpdate{
				Watched: false,
			},
		},
		{
			expectedStatusCode: http.StatusConflict,
			requestBody: ReleaseUpdate{
				Watched: false,
			},
			responseBody: routes.WatchedDidNotChange,
		},
	}

	RunScenarios(scenarios, t)
}

func TestMain(m *testing.M) {
	s = new(TestServer)

	if err := s.Run(); err != nil {
		log.Fatal(err)
	}

	code := m.Run()

	if err := s.CleanDatabase(); err != nil {
		log.Fatal(err)
	}

	os.Exit(code)
}
