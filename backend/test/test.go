package test

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/eemj/anime-tracker/backend/models"
	"gitlab.com/eemj/anime-tracker/backend/server"
)

var endpoint string

func GetAvailablePort() string {
	port := 8080
	for {
		if port >= 65535 {
			port = 500
			continue
		}

		if conn, err := net.Listen("tcp", fmt.Sprintf("127.0.0.1:%d", port)); err != nil {
			port++
			continue
		} else {
			conn.Close()
			return strconv.Itoa(port)
		}
	}
}

func EnsurePort() (err error) {
	if port := os.Getenv("PORT"); port == "" {
		err = os.Setenv("PORT", GetAvailablePort())
	}
	return
}

type TestServer struct {
	srv *server.Server
}

func (s *TestServer) CleanDatabase() (err error) {
	if err = s.srv.DB.DropTableIfExists(models.Anime{}).Error; err != nil {
		return
	}
	if err = s.srv.DB.DropTableIfExists(models.Release{}).Error; err != nil {
		return
	}

	s.srv.AutoMigrate()

	return
}

func (s *TestServer) IsRunning(done chan<- bool) {
	go func() {
		for {
			res, err := http.Get(endpoint + "/api/anime")

			defer res.Body.Close()

			if err != nil {
				log.Fatal(err)
			}

			time.Sleep(100 * time.Millisecond)

			if res.StatusCode == http.StatusOK {
				done <- true
				break
			}
		}
	}()
}

func (s *TestServer) Run() (err error) {
	if err = EnsurePort(); err != nil {
		return
	}

	var srv *server.Server

	if srv, err = server.NewServer(server.DBConnection{}); err != nil {
		return
	}

	endpoint = fmt.Sprintf("http://127.0.0.1:%s", srv.Port)

	(*s).srv = srv

	if err = s.CleanDatabase(); err != nil {
		return
	}

	done := make(chan bool, 1)

	go func() {
		if err = s.srv.Run(); err != nil {
			return
		}

		defer s.srv.Server.Close()
	}()

	s.IsRunning(done)
	<-done

	return
}
