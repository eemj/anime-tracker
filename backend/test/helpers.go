package test

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	_ "strconv"
	"strings"
	"testing"
	"time"
)

var dummyAnime = []AnimeCreation{
	AnimeCreation{
		Name:  "One Piece",
		Link:  "one-piece",
		Image: "http://something.com/one-piece.jpg",
	},
	AnimeCreation{
		Name:  "Naruto",
		Link:  "naruto",
		Image: "http://something.com/naruto.jpg",
	},
}

var dummyRelease = []ReleaseCreation{
	ReleaseCreation{
		Episode:      "1",
		DateReleased: time.Now().Unix(),
	},
	ReleaseCreation{
		Episode:      "2",
		DateReleased: time.Now().Unix(),
	},
	ReleaseCreation{
		Episode:      "3",
		DateReleased: time.Now().Unix(),
	},
	ReleaseCreation{
		Episode:      "4",
		DateReleased: time.Now().Unix(),
	},
}

func request(uri, method string, params ...string) (res *http.Response, body map[string]interface{}, err error) {
	req := &http.Request{}
	if len(params) > 0 {
		req, err = http.NewRequest(method, uri, strings.NewReader(params[0]))
	} else {
		req, err = http.NewRequest(method, uri, nil)
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		return
	}

	res, err = http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	var bytes []byte
	if bytes, err = ioutil.ReadAll(res.Body); err != nil {
		return
	}

	body = make(map[string]interface{})
	if err = json.Unmarshal(bytes, &body); err != nil {
		return
	}

	return
}

func createAndReturnID(endpoint string, form string) (id uint64, res *http.Response, err error) {
	res, body, err := request(endpoint, http.MethodPost, form)

	if err != nil {
		return
	}

	if body["error"] != nil {
		err = errors.New(body["error"].(string))
		return
	}

	id = uint64(body["data"].(map[string]interface{})["id"].(float64))

	return
}

func createAnime(animeCreation AnimeCreation) (id uint64, res *http.Response, err error) {
	id, res, err = createAndReturnID(fmt.Sprintf("%s/api/anime", endpoint), animeCreation.IntoForm())
	return
}

func createRelease(animeID uint64, releaseCreation ReleaseCreation) (id uint64, res *http.Response, err error) {
	id, res, err = createAndReturnID(fmt.Sprintf("%s/api/anime/%d/release", endpoint, animeID), releaseCreation.IntoForm())
	return
}

func updateAndReturnError(endpoint, form string) (res *http.Response, err error) {
	res, body, err := request(endpoint, http.MethodPut, form)

	if err != nil {
		return
	}

	if body["error"] != nil {
		err = errors.New(body["error"].(string))
		return
	}

	return
}

func updateAnime(animeID uint64, animeUpdate AnimeUpdate) (res *http.Response, err error) {
	res, err = updateAndReturnError(fmt.Sprintf("%s/api/anime/%d", endpoint, animeID), animeUpdate.IntoForm())
	return
}

func updateRelease(animeID, releaseID uint64, releaseUpdate ReleaseUpdate) (res *http.Response, err error) {
	res, err = updateAndReturnError(fmt.Sprintf("%s/api/anime/%d/release/%d", endpoint, animeID, releaseID), releaseUpdate.IntoForm())
	return
}

func deleteAndReturnError(endpoint string) (res *http.Response, err error) {
	res, body, err := request(endpoint, http.MethodDelete)

	if err != nil {
		return
	}

	if body["error"] != nil {
		err = errors.New(body["error"].(string))
		return
	}

	return
}

func deleteAnime(animeID uint64) (res *http.Response, err error) {
	res, err = deleteAndReturnError(fmt.Sprintf("%s/api/anime/%d", endpoint, animeID))
	return
}

func deleteRelease(animeID, releaseID uint64) (res *http.Response, err error) {
	res, err = deleteAndReturnError(fmt.Sprintf("%s/api/anime/%d/release/%d", endpoint, animeID, releaseID))
	return
}

func createDummyAnimes(animeIDs *[]uint64, t *testing.T) {
	if len(*animeIDs) == 0 {
		for _, anime := range dummyAnime {
			id, _, err := createAnime(anime)

			if err != nil {
				t.Fatal(err)
			}

			*animeIDs = append(*animeIDs, id)
		}
	}
}

func createDummyRelease(animeIDs, releaseIDs *[]uint64, t *testing.T) {
	if len(*releaseIDs) == 0 {
		for _, release := range dummyRelease {
			id, _, err := createRelease((*animeIDs)[0], release)

			if err != nil {
				t.Fatal(err)
			}

			*releaseIDs = append(*releaseIDs, id)
		}
	}
}

func RunScenarios(scenarios []TestScenario, t *testing.T) {
	animeIDs := make([]uint64, 0, len(dummyAnime))
	releaseIDs := make([]uint64, 0, len(dummyRelease))

	for index, scenario := range scenarios {
		t.Logf("%d.) %+v\n", index, scenario)

		var (
			res *http.Response
			err error
		)

		if strings.Contains(t.Name(), "Create") {
			if strings.HasSuffix(t.Name(), "Anime") {
				_, res, err = createAnime(scenario.requestBody.(AnimeCreation))
			} else if strings.HasSuffix(t.Name(), "Release") {
				createDummyAnimes(&animeIDs, t)

				_, res, err = createRelease(animeIDs[index%2], scenario.requestBody.(ReleaseCreation))
			}
		} else if strings.Contains(t.Name(), "Update") {
			createDummyAnimes(&animeIDs, t)

			if strings.HasSuffix(t.Name(), "Anime") {
				res, err = updateAnime(animeIDs[index%2], scenario.requestBody.(AnimeUpdate))
			} else if strings.HasSuffix(t.Name(), "Release") {
				createDummyRelease(&animeIDs, &releaseIDs, t)

				t.Logf("Updating release %d\n", releaseIDs[index%2])

				res, err = updateRelease(animeIDs[0], releaseIDs[0], scenario.requestBody.(ReleaseUpdate))
			}
		} else if strings.Contains(t.Name(), "Delete") {
			createDummyAnimes(&animeIDs, t)
			createDummyRelease(&animeIDs, &releaseIDs, t)

			if strings.HasSuffix(t.Name(), "Anime") {
				if index == 2 { // if it's index 2, it means that the responseBody should be the anime does not exist
					res, err = deleteAnime(9999)
				} else {
					res, err = deleteAnime(animeIDs[index])
				}
			} else if strings.HasSuffix(t.Name(), "Release") {
				if index == 3 {
					res, err = deleteRelease(animeIDs[0], 9999)
				} else if index == 2 {
					res, err = deleteRelease(9999, releaseIDs[1])
				} else {
					res, err = deleteRelease(animeIDs[0], releaseIDs[index])
				}
			}
		}

		if err != nil && err.Error() != scenario.responseBody {
			t.Fatalf("Expected: %s\nFound: %s\n", scenario.responseBody, err.Error())
		}

		if res.StatusCode != scenario.expectedStatusCode {
			t.Fatalf("Expected: %d\nFound: %d\n", scenario.expectedStatusCode, res.StatusCode)
		}
	}
}
