package routes

import "encoding/json"

type APIResponse struct {
	Error string      `json:"error,omitempty"`
	Code  int         `json:"code,omitempty"`
	Data  interface{} `json:"data,omitempty"`
}

func (a *APIResponse) MarshalJSON() ([]byte, error) {
	if a.Error != "" && a.Data == nil {
		return json.Marshal(struct {
			Error string `json:"error"`
			Code  int    `json:"code"`
		}{
			Error: a.Error,
			Code:  a.Code,
		})
	}
	return json.Marshal(struct {
		Data interface{} `json:"data"`
	}{
		Data: a.Data,
	})
}
