package routes

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/eemj/anime-tracker/backend/models"
)

func (rt *Routes) ReleaseCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		releaseID := parseURI(r, "releaseID")
		anime := r.Context().Value("anime").(*models.Anime)
		release, err := models.GetRelease(anime.ID, releaseID)

		if err != nil {
			rt.resError(w, http.StatusNotFound, ReleaseDoesNotExist)
			return
		}

		ctx := context.WithValue(r.Context(), "release", release)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (rt *Routes) getReleases(w http.ResponseWriter, r *http.Request) {
	anime := r.Context().Value("anime").(*models.Anime)
	releases := models.GetReleases(models.Release{AnimeID: anime.ID}, 0, 0)
	rt.resJSON(w, http.StatusOK, releases)
}

func (rt *Routes) createRelease(w http.ResponseWriter, r *http.Request) {
	episode := r.FormValue("episode")
	dateReleased := r.FormValue("date_released")

	if episode == "" {
		rt.resError(w, http.StatusBadRequest, MissingEpisode)
		return
	} else if dateReleased == "" {
		rt.resError(w, http.StatusBadRequest, MissingDateReleased)
		return
	}

	// attempting to parse the date
	date, err := strconv.ParseUint(dateReleased, 10, 64)

	if err != nil {
		rt.resError(w, http.StatusBadRequest, DateIsNotUnsigned)
		return
	}

	anime := r.Context().Value("anime").(*models.Anime)

	newRelease := &models.Release{
		AnimeID: anime.ID,
		Episode: episode,
		Watched: &[]bool{false}[0],
	}

	if releases := models.GetReleases(*newRelease, 0, 0); len(*releases) > 0 {
		rt.resError(w, http.StatusConflict, ReleaseExists)
		return
	}

	(*newRelease).DateReleased = date

	if err := models.CreateRelease(newRelease); err != nil {
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rt.resJSON(w, http.StatusCreated, *newRelease)
}

func (rt *Routes) getRelease(w http.ResponseWriter, r *http.Request) {
	release := r.Context().Value("release").(*models.Release)

	rt.resJSON(w, http.StatusOK, *release)
}

func (rt *Routes) deleteRelease(w http.ResponseWriter, r *http.Request) {
	release := r.Context().Value("release").(*models.Release)

	if err := models.DeleteRelease(*release); err != nil {
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rt.resJSON(w, http.StatusAccepted, "OK")
}

func (rt *Routes) updateRelease(w http.ResponseWriter, r *http.Request) {
	release := r.Context().Value("release").(*models.Release)

	watched := r.FormValue("watched")

	if !(watched == "true" || watched == "false") {
		rt.resError(w, http.StatusBadRequest, WatchedIsNotBoolean)
		return
	}

	value, _ := strconv.ParseBool(watched)

	if *release.Watched == value {
		rt.resError(w, http.StatusConflict, WatchedDidNotChange)
		return
	}

	(*release).Watched = &value
	(*release).WatchedDate = uint64(time.Now().Unix())

	if err := models.UpdateRelease(release); err != nil {
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rt.resJSON(w, http.StatusAccepted, *release)
}
