package routes

import (
	"log"
	"net/http"
)

const NotFound string = "Not Found"
const MethodNotAllowed string = "Method Not Allowed"

func (rt *Routes) resJSON(w http.ResponseWriter, code int, payload interface{}) {
	res := APIResponse{Data: payload}
	jsonPayload, err := res.MarshalJSON()

	if err != nil {
		log.Printf("@resJSON: %s\n", err)
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonPayload)
}

func (rt *Routes) resError(w http.ResponseWriter, code int, message string) {
	if code == http.StatusInternalServerError {
		// hide the message, we don't want to show the users what went wrong.
		if message != "" {
			log.Println(message)
		}
		message = "Internal Server Error"
	}

	res := APIResponse{Code: code, Error: message}
	jsonPayload, err := res.MarshalJSON()

	if err != nil {
		log.Printf("@resError: %s\n", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonPayload)
}

func (rt *Routes) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	rt.resError(w, http.StatusNotFound, NotFound)
}

func (rt *Routes) notAllowedHandler(w http.ResponseWriter, r *http.Request) {
	rt.resError(w, http.StatusMethodNotAllowed, MethodNotAllowed)
}
