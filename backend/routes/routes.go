package routes

import (
	"net/http"
	"path/filepath"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

const frontend string = "../frontend/dist"
const animeRedirect string = "/api/anime"

type Routes struct {
	DB *gorm.DB
}

func redirectHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, animeRedirect, http.StatusMovedPermanently)
}

func (rt *Routes) New() (router *chi.Mux) {
	router = chi.NewRouter()

	router.Use(
		middleware.RequestID,
		middleware.RealIP,
		middleware.Logger,
		middleware.Recoverer,
		middleware.Timeout(60*time.Second),
	)

	for _, path := range []string{
		"/",
		"/js",
		"/css",
		"/img",
	} {
		router.Mount(path, http.StripPrefix(
			path, http.FileServer(http.Dir(filepath.Join(frontend, path))),
		))
	}

	router.Route("/api", func(r chi.Router) {
		r.Get("/", redirectHandler)

		r.Route("/anime", func(r chi.Router) {
			r.Get("/", rt.getAnimes)
			r.Post("/", rt.createAnime)

			r.Route("/{animeID:[0-9]+}", func(r chi.Router) {
				r.Use(rt.AnimeCtx)

				r.Get("/", rt.getAnime)
				r.Delete("/", rt.deleteAnime)
				r.Put("/", rt.updateAnime)

				r.Route("/release", func(r chi.Router) {
					r.Get("/", rt.getReleases)
					r.Post("/", rt.createRelease)

					r.Route("/{releaseID:[0-9]+}", func(r chi.Router) {
						r.Use(rt.ReleaseCtx)

						r.Get("/", rt.getRelease)
						r.Delete("/", rt.deleteRelease)
						r.Put("/", rt.updateRelease)
					})
				})
			})
		})
	})

	router.NotFound(rt.notFoundHandler)
	router.MethodNotAllowed(rt.notAllowedHandler)

	return
}
