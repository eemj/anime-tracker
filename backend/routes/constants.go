package routes

// Constant Messages

const (
	// Missing form values
	MissingDateReleased string = "`date_released` is missing"
	MissingName         string = "`name` is missing"
	MissingEpisode      string = "`episode` is missing"
	MissingLink         string = "`link` is missing"
	MissingImage        string = "`image` is missing"
	MissingAnime        string = "`anime` is missing"
	MissingStatus       string = "`status` is missing"

	// Wrong validation
	ImageIsNotValidURI string = "`image` is not a valid uri"

	// Entity already exists/missing
	AnimeExists         string = "This anime already exists"
	ReleaseExists       string = "This release already exists"
	AnimeDoesNotExist   string = "This anime does not exist"
	ReleaseDoesNotExist string = "This release does not exist"

	// Unsigned error messages
	IdIsNotUnsigned   string = "`id` is not an unsigned integer"
	DateIsNotUnsigned string = "`date` is not an unsigned integer"

	// Others
	OneQueryAtATime     string = "You can only specify one query at a time"
	WatchedIsNotBoolean string = "`watched` does not contain a boolean"
	StatusDidNotChange  string = "`status` did not change"
	WatchedDidNotChange string = "`watched` did not change"
)
