package routes

import (
	"context"
	"net/http"
	"regexp"
	"strconv"

	"github.com/go-chi/chi"

	"gitlab.com/eemj/anime-tracker/backend/models"
)

var imageValidation = regexp.MustCompile(`^https?:\/\/.+\.(jpe?g|png|gif)$`)

func parseURI(r *http.Request, name string) (value uint64) {
	value, _ = strconv.ParseUint(chi.URLParam(r, name), 10, 64)
	return
}

func (rt *Routes) AnimeCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		animeID := parseURI(r, "animeID")
		anime, err := models.GetAnime(animeID)

		if err != nil {
			rt.resError(w, http.StatusNotFound, AnimeDoesNotExist)
			return
		}

		ctx := context.WithValue(r.Context(), "anime", anime)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (rt *Routes) getAnimes(w http.ResponseWriter, r *http.Request) {
	animes := models.GetAnimes(models.Anime{})
	rt.resJSON(w, http.StatusOK, animes)
}

func (rt *Routes) createAnime(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	link := r.FormValue("link")
	image := r.FormValue("image")

	if name == "" {
		rt.resError(w, http.StatusBadRequest, MissingName)
		return
	} else if link == "" {
		rt.resError(w, http.StatusBadRequest, MissingLink)
		return
	} else if image == "" {
		rt.resError(w, http.StatusBadRequest, MissingImage)
		return
	} else if !imageValidation.MatchString(image) {
		rt.resError(w, http.StatusBadRequest, ImageIsNotValidURI)
		return
	}

	newAnime := models.Anime{Name: name, Link: link, Image: image}

	if err := models.CreateAnime(&newAnime); err != nil {
		rt.resError(w, http.StatusConflict, AnimeExists)
		return
	}

	rt.resJSON(w, http.StatusCreated, newAnime)
}

func (rt *Routes) getAnime(w http.ResponseWriter, r *http.Request) {
	anime := r.Context().Value("anime").(*models.Anime)

	rt.resJSON(w, http.StatusOK, *anime)
}

func (rt *Routes) deleteAnime(w http.ResponseWriter, r *http.Request) {
	anime := r.Context().Value("anime").(*models.Anime)

	if err := models.DeleteAnime(*anime); err != nil {
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rt.resJSON(w, http.StatusAccepted, "OK")
}

func (rt *Routes) updateAnime(w http.ResponseWriter, r *http.Request) {
	anime := r.Context().Value("anime").(*models.Anime)

	status := r.FormValue("status")

	if status == "" {
		rt.resError(w, http.StatusBadRequest, MissingStatus)
		return
	} else if anime.Status == status {
		rt.resError(w, http.StatusConflict, StatusDidNotChange)
		return
	}

	(*anime).Status = status

	if err := models.UpdateAnime(anime); err != nil {
		rt.resError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rt.resJSON(w, http.StatusAccepted, *anime)
}
