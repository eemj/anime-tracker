module gitlab.com/eemj/anime-tracker/backend

require (
	github.com/go-chi/chi v3.3.3+incompatible
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
)
