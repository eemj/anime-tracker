#!/usr/bin/env python3

# Implying the database doesn't have any data and was just created

import os
import requests
import random
import time
import json

CREATED = 201


def image(link: str) -> str:
    return f"http://www.anime1.com/main/img/content/{link}/{link}-210.jpg"


animes = [{
    "name": "One Piece",
    "link": "one-piece",
}, {
    "name": "Naruto",
    "link": "naruto",
}, {
    "name": "One Punch Man",
    "link": "one-punch-man",
}, {
    "name": "3D Kanojo: Real Girl",
    "link": "3d-kanojo-real-girl",
},
    {
    "name": "Toaru Majutsu no Index III",
    "link": "toaru-majutsu-no-index-iii",
}, {
    "name": "Yagate Kimi ni Naru",
    "link": "yagate-kimi-ni-naru",
},
    {
    "name":
    "Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai",
    "link":
    "seishun-buta-yarou-wa-bunny-girl-senpai-no-yume-wo-minai",
},
    {
    "name": "Ze Tian Ji 2nd Season",
    "link": "ze-tian-ji-2nd-season",
}, {
    "name": "Zegapain",
    "link": "zegapain",
},
    {
    "name": "Zero kara Hajimeru Mahou no Sho",
    "link": "zero-kara-hajimeru-mahou-no-sho",
}, {
    "name": "Zero no Tsukaima",
    "link": "zero-no-tsukaima",
}, {
    "name": "Zero no Tsukaima F",
    "link": "zero-no-tsukaima-f",
},
    {
    "name": "Zero no Tsukaima: Futatsuki no Kishi",
    "link": "zero-no-tsukaima-futatsuki-no-kishi",
},
    {
    "name": "Zero no Tsukaima: Princesses no Rondo",
    "link": "zero-no-tsukaima-princesses-no-rondo",
},
    {
    "name": "Zero no Tsukaima: Princesses no Rondo Special",
    "link": "zero-no-tsukaima-princesses-no-rondo-special",
}, {
    "name": "Zetman",
    "link": "zetman",
}, {
    "name": "Zetsuen no Tempest",
    "link": "zetsuen-no-tempest",
},
    {
    "name": "Zetsumetsu Kigu Shoujo: Amazing Twins",
    "link": "zetsumetsu-kigu-shoujo-amazing-twins",
},
    {
    "name": "Zettai Bouei Leviathan",
    "link": "zettai-bouei-leviathan",
},
    {
    "name": "Zettai Karen Children: The Unlimited - Hyoubu Kyousuke",
    "link": "zettai-karen-children-the-unlimited-hyoubu-kyousuke",
}, {
    "name": "Zettai Shounen",
    "link": "zettai-shounen",
}, {
    "name": "Zoids",
    "link": "zoids",
}, {
    "name": "Zoids Fuzors",
    "link": "zoids-fuzors",
}, {
    "name": "Zoids Wild",
    "link": "zoids-wild",
},
    {
    "name": "Zoku Natsume Yuujinchou",
    "link": "zoku-natsume-yuujinchou",
},
    {
    "name": "Zoku Sayonara Zetsubou Sensei",
    "link": "zoku-sayonara-zetsubou-sensei",
},
    {
    "name": "Zoku Touken Ranbu: Hanamaru",
    "link": "zoku-touken-ranbu-hanamaru",
}, {
    "name": "Zombie-Loan",
    "link": "zombie-loan",
}, {
    "name": "Zombie-Loan Specials",
    "link": "zombie-loan-specials",
}, {
    "name": "Zombieland Saga",
    "link": "zombieland-saga",
},
    {
    "name": "Zutto Mae kara Suki deshita.: Kokuhaku Jikkou Iinkai",
    "link": "zutto-mae-kara-suki-deshita-kokuhaku-jikkou-iinkai",
}]

port = os.getenv("PORt")

if not port:
    port = "8999"

if __name__ == "__main__":
    today = round(time.time())
    last_release = 0

    for index, anime in enumerate(animes):
        anime['image'] = image(anime['link'])
        r = requests.post(f"http://127.0.0.1:{port}/api/anime", data=anime)

        if r.status_code != CREATED:
            continue

        if random.randint(0, 1) == 0:
            continue

        episodes = random.randint(1, 200)

        for release in range(0, episodes):
            r = requests.post(
                f"http://127.0.0.1:{port}/api/anime/{index + 1}/release",
                data={
                    'episode': (release + 1),
                    'date_released': today,
                })

            if r.status_code == CREATED:
                body = json.loads(r.text)
                if release % 2 == 0:
                    last_release = body['data']['id']
                    r = requests.put(
                        f"http://127.0.0.1:{port}/api/anime/{index + 1}/release/{last_release}",
                        data={
                            'watched': 'true',
                            'watched_date': today,
                        })

                    print(r.text)
