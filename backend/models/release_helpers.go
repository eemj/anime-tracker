package models

func GetRelease(animeID, releaseID uint64) (release *Release, err error) {
	release = &Release{}
	err = db.Where(Release{ID: releaseID, AnimeID: animeID}).Find(&release).Error
	return
}

func GetReleases(query Release, pageSize, pageNum int) (releases *[]Release) {
	releases = &[]Release{}
	if pageSize > 0 && pageNum > 0 {
		db.Where(&query).Offset(pageNum).Limit(pageSize).Find(&releases)
	} else {
		db.Where(&query).Find(&releases)
	}
	return
}

func CreateRelease(newRelease *Release) (err error) {
	err = db.Create(newRelease).Error
	return
}

func UpdateRelease(updatedRelease *Release) (err error) {
	err = db.Model(Release{}).Select("watched", "watched_date").Where(Release{ID: (*updatedRelease).ID}).Update(&updatedRelease).Error
	return
}

func DeleteRelease(deleteRelease Release) (err error) {
	err = db.Where(&deleteRelease).Delete(Release{}).Error
	return
}
