package models

type Anime struct {
	ID     uint64 `json:"id"`
	Name   string `gorm:"not null;unique" json:"name"`
	Link   string `gorm:"not null;unique;index:idx_anime_name" json:"link"`
	Image  string `gorm:"not null;unique" json:"image"`
	Status string `json:"status,omitempty"`
}

func (Anime) TableName() string {
	return "animes"
}
