package models

type Release struct {
	ID           uint64 `gorm:"primary_key" json:"id"`
	Anime        Anime  `gorm:"foreignkey:AnimeID" json:"-"`
	AnimeID      uint64 `json:"-"`
	WatchedDate  uint64 `json:"watched_date,omitempty"`
	DateReleased uint64 `gorm:"not null" json:"date_released"`
	Watched      *bool  `json:"watched"`
	Episode      string `gorm:"not null" json:"episode"`
}

func (Release) TableName() string {
	return "releases"
}
