package models

func GetAnime(ID uint64) (anime *Anime, err error) {
	anime = &Anime{}
	err = db.Where(Anime{ID: ID}).Find(&anime).Error
	return
}

func GetAnimes(query Anime) (animes *[]Anime) {
	animes = &[]Anime{}
	db.Where(&query).Find(&animes)
	return
}

func CreateAnime(newAnime *Anime) (err error) {
	err = db.Create(&newAnime).Error
	return
}

func UpdateAnime(updatedAnime *Anime) (err error) {
	err = db.Save(&updatedAnime).Error
	return
}

func DeleteAnime(deleteAnime Anime) (err error) {
	err = db.Where(Release{AnimeID: deleteAnime.ID}).Delete(Anime{}).Error
	err = db.Delete(&deleteAnime).Error
	return
}
