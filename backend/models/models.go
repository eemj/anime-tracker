package models

import "github.com/jinzhu/gorm"

var db *gorm.DB

func Init(d *gorm.DB) {
	db = d
}
