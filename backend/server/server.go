package server

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"

	"gitlab.com/eemj/anime-tracker/backend/models"
	"gitlab.com/eemj/anime-tracker/backend/routes"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Server struct {
	DB     *gorm.DB
	Router *chi.Mux
	Server *http.Server
	Port   string
}

func (s *Server) Run() error {
	return s.Server.ListenAndServe()
}

func (s *Server) AutoMigrate() {
	s.DB.AutoMigrate(models.Release{}, models.Anime{})
}

type DBConnection struct {
	Host, Port, User, DBName, Password string
}

func (d DBConnection) connectionString() (string, string) {
	if d.Host != "" && d.User != "" && d.Password != "" && d.DBName != "" && d.Port != "" {
		return "postgres", fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			d.Host, d.Port, d.User, d.Password, d.DBName,
		)
	}
	return "sqlite3", "/tmp/anime-tracker.db"
}

func NewServer(connection DBConnection) (server *Server, err error) {
	dialect, host := connection.connectionString()

	db, err := gorm.Open(dialect, host)

	if err != nil {
		return server, err
	}

	server = &Server{DB: db}

	router := routes.Routes{DB: db}
	models.Init(db)

	server.Router = router.New()
	server.Port = os.Getenv("PORT")
	server.Server = &http.Server{
		Addr:    fmt.Sprintf(":%s", server.Port),
		Handler: chi.ServerBaseContext(context.Background(), (*server).Router),
	}

	server.AutoMigrate()

	if len(os.Getenv("DEBUG")) > 0 {
		server.DB.LogMode(true)
	}

	return server, err
}
