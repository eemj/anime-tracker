import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home.vue'
import Release from '@/views/Release.vue'
import NotFound from '@/views/NotFound.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/release/:animeId',
      name: 'release',
      component: Release,
    },
    {
      path: '*',
      name: '404',
      component: NotFound,
    },
  ],
})
