import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const watchedFormData = new window.FormData()
watchedFormData.append('watched', 'true')

export default new Vuex.Store({
  state: {
    animes: [],
    error: '',
  },
  mutations: {
    setAnimes: (state, animes) => (state.animes = animes.map(a => ({ ...a, ...{ releases: [] } }))),
    setReleases: (state, { animeId, releases }) => {
      const anime = state.animes.find(({ id }) => animeId === id)
      if (anime) anime.releases = releases
    },
    setError: (state, error) => (state.error = error),
    setWatched: (state, { animeId, releaseId }) => {
      const anime = state.animes.find(({ id }) => animeId === id)
      if (anime) {
        const release = anime.releases.find(({ id }) => releaseId === id)
        if (release) release.watched = true
      }
    },
  },
  getters: {
    unwatchedCount: state => {
      const releases = state.animes.map(anime => (anime.releases ? anime.releases.length : 0))
      if (releases.length) return releases.reduce((a, b) => a + b)
      return 0
    },
    animeReleases: state => animeId => {
      const anime = state.animes.find(({ id }) => id === parseInt(animeId, 10))
      if (anime) return anime.releases
      return []
    },
    unwatched: state => animeId => {
      const anime = state.animes.find(({ id }) => id === parseInt(animeId, 10))
      if (anime) return anime.releases.length
      return 0
    },
    animeFromId: state => animeId => {
      return state.animes.find(({ id }) => id === parseInt(animeId, 10))
    }
  },
  actions: {
    getAllAnimes: ({ commit, state }) => {
      fetch('/api/anime')
        .then(res => res.json())
        .then(({ data: animes }) => {
          commit('setAnimes', animes)

          for (const anime of state.animes) {
            new Promise(resolve => {
              fetch(`/api/anime/${anime.id}/release`)
                .then(res => res.json())
                .then(({ data: releases }) => {
                  commit('setReleases', { animeId: anime.id, releases })
                  resolve()
                })
                .catch(error => {
                  console.error(error)
                  state.error = error
                })
            })
          }
        })
        .catch(error => {
          console.error(error)
          state.error = error
        })
    },
    markWatched: ({ commit }, { animeId, releaseId }) => {
      fetch(`/api/anime/${animeId}/release/${releaseId}`, { method: 'PUT', body: watchedFormData })
        .then(res => res.json())
        .then(({ error }) => {
          if (!error) {
            commit('setWatched', { animeId, releaseId })
          }
        })
        .catch(error => {
          console.error(error)
        })
    },
  },
})
